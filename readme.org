#+TITLE:Functional Mandelbrot in R

#+options: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+options: author:t broken-links:nil c:nil creator:nil
#+options: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+options: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+options: timestamp:t title:t toc:nil todo:t |:t

#+author: Jason Cairns
#+email: jcai849@aucklanduni.ac.nz
#+language: en
#+select_tags: export
#+exclude_tags: noexport
#+creator: Emacs 26.1 (Org mode 9.2.3)

#+latex_class: article
#+LATEX_CLASS_OPTIONS: [a4paper, 11pt]
#+LATEX_HEADER: \usepackage{natbib}
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage{mathtools}
#+LATEX_HEADER: \usepackage{amsfonts}
#+latex_header_extra:
#+description:
#+keywords:
#+subtitle:
#+date: \today


* Introduction
This is my attempt to understand the Mandelbrot set in a more
mathematical than algorithmic sense through R, written entirely
functionally (no loops etc.)

* Mandelbrot Set
From Wikipedia,
#+begin_quote
The Mandelbrot set is the set of complex numbers \(c\) for which the
function \(f_{c}(z)=z^{2}+c\) does not diverge when iterated from
\(z=0\)
#+end_quote
More formally, this can also be represented as,
\begin{equation}
z_{n+1}=z_{n}^{2}+c
\end{equation}
Where
\begin{equation}
c\in M\iff \limsup _{n\to \infty }|z_{n+1}|\leq 2
\end{equation}

* R Implementation
I will base my implementation on the graphics I have seen which
correspond x,y pixels to complex pairs, coloured by the sequence index
for which they diverge. First, to get a feel for it, I will create a
function that tests for divergence (and approximate membership) in
\(n\) iterations of the sequence. From there, I will modify the
function to perform as the graphics I have seen.
\newpage
** Membership
#+begin_src R :results output :colnames yes :hline yes :session rsession1 :tangle yes :comments link :exports both :eval never-export
  library(ggplot2)
  library(magrittr)

  #' inM determines membership in the Mandelbrot set of a complex number over n iterations
  #'
  #' @param c complex number to test membership over
  #' 
  #' @param max_n maximum number of iterations
  #' 
  #' @param z complex sequential component; for classical mandelbrot set, start at 0
  #' 
  #' @param n number of iterations (not to be altered by hand)
  #'
  #' @return boolean indicating membership in Mandelbrot set
  inM <- function(c, max_n=30, z=0, n=1){
    ifelse(abs(z) > 2,
	   FALSE,
	   ifelse(n == max_n,
		  TRUE,
		  inM(c, max_n, z^2 + c, n+1)))
  }
#+end_src

#+RESULTS:

To generate data for this function, we use the following function to
create a dataframe that covers the range and increment distance over
the complex space specified. It creates three columns; =re= and =im=
being numeric for the purpose of plotting, and =c= being their
complex equivalent, to run the previous function over.

#+begin_src R :results output :colnames yes :hline yes :session rsession1 :tangle yes :comments link :exports both :eval never-export
  #' generates a dataframe grid of complex numbers
  #'
  #' @param refrom lower bound of the real components
  #'
  #' @param reto upper bound of the real components
  #'
  #' @param reby increment amount of the real components
  #'
  #' @param imfrom lower bound of the imaginary components
  #'
  #' @param imto upper bound of the imaginary components
  #'
  #' @param imby increment amount of the imaginary components
  #'
  #' @return dataframe with three columns; "re" numeric, "im" numeric,
  #'   and "c" being their complex equivalent
  complex_df <- function(refrom, reto, reby, imfrom, imto, imby){
    expand.grid(re=seq(refrom, reto, reby), im=seq(imfrom, imto, imby)) %>%
      dplyr::mutate(c = re+im*1i)
  }
#+end_src

#+RESULTS:

\newpage
We can implement this through the following code:

#+begin_src R :file inM-overall.png :res 100 :height 600 :width 600 :results output graphics :colnames yes :session rsession1 :exports both :eval never-export
max_n <- 20

  complex_df(-2.5, 1, 0.01, -1.5, 1.5, 0.01) %>%
    dplyr::mutate(inM = purrr::map_lgl(c, inM, max_n)) %>%
    ggplot(aes(re, im)) +
    geom_point(aes(colour = inM)) +
    scale_colour_manual(values = c("TRUE" = "black", "FALSE" = "white")) +
    coord_fixed() +
    labs(title = "Mandelbrot Set",
	 x = "Real",
	 y = "Imaginary") + 
    theme(legend.position = "none")
#+end_src

#+RESULTS:
[[file:inM-overall.png]]

The results are excellent, though the code ran a little slow (to be
expected, I just want it working, not optimised). The following
demonstrates a detail at the complex rectangle \([-1+0i, 0.5+0.5i]\)
\newpage
#+begin_src R :file inM-detail.png :res 100 :height 600 :width 600 :results output graphics :colnames yes :session rsession1 :exports both :eval never-export
  max_n <- 25

  complex_df(-1, -0.5, 0.001, 0, 0.5, 0.001) %>%
    dplyr::mutate(inM = purrr::map_lgl(c, inM, max_n)) %>%
    ggplot(aes(re, im)) +
    geom_point(aes(colour = inM)) +
    scale_colour_manual(values = c("TRUE" = "black", "FALSE" = "white")) +
    coord_fixed() +
    labs(title = "Mandelbrot Set Detail",
	 x = "Real",
	 y = "Imaginary") + 
    theme(legend.position = "none")
#+end_src

#+RESULTS:
[[file:inM-detail.png]]
\newpage
** Divergence Iteration
We will now take the function further, returning not boolean
TRUE/FALSE values, but the number of iterations until divergence. This
is actually more truthful, as divergence can't be tested for with any
finite amount of iterations, thus TRUE/FALSE is just an approximation,
or "doesn't diverge within =max_n= iterations".
#+begin_src R :results output :colnames yes :hline yes :session rsession1 :tangle yes :comments link :exports both :eval never-export
  #' nM counts the number of iterations until divergence for the Mandelbrot set
  #'
  #' @param c complex number to test membership over
  #' 
  #' @param max_n maximum number of iterations
  #' 
  #' @param z complex sequential component; for classical mandelbrot
  #'   set, start at 0
  #' 
  #' @param n number of iterations (not to be altered by hand)
  #'
  #' @return integer indicating number of iterations until divergence
  #'   (or maximum number of iterations allowed)
  nM <- function(c, max_n=30, z=0, n=1){
    ifelse(abs(z) > 2 | n == max_n,
	   as.integer(n),
	   nM(c, max_n, z^2 + c, n+1))
  }
#+end_src

#+RESULTS:

With the corresponding implementation:

#+begin_src R :file nM-overall.png :res 100 :height 400 :width 600 :results output graphics :colnames yes :session rsession1 :exports both :eval never-export
  max_n <- 20

  complex_df(-2.5, 1, 0.01, -1.5, 1.5, 0.01) %>%
    dplyr::mutate(nM = purrr::map_int(c, nM, max_n)) %>%
    ggplot(aes(re, im)) +
    geom_point(aes(colour = nM)) +
    scale_colour_viridis_c(option = "plasma") +
    coord_fixed() +
    labs(title = "Mandelbrot Set",
	 caption = glue::glue("Note: Maximum number of iterations: {max_n}.\nDivergence forced at maximum iterations."),
	 x = "Real", y = "Imaginary",
	 colour = "Iterations\nuntil divergence")
#+end_src

#+RESULTS:
[[file:nM-overall.png]]


#+begin_src R :file nM-detail.png :res 100 :height 400 :width 600 :results output graphics :colnames yes :session rsession1 :exports both :eval never-export
max_n <- 25

  complex_df(-1, -0.5, 0.001, 0, 0.5, 0.001) %>%
    dplyr::mutate(nM = purrr::map_int(c, nM, max_n)) %>%
    ggplot(aes(re, im)) +
    geom_point(aes(colour = nM)) +
    scale_colour_viridis_c(option = "plasma") +
    coord_fixed() +
    labs(title = "Mandelbrot Set",
	 caption = glue::glue("Note: Maximum number of iterations: {max_n}.\nDivergence forced at maximum iterations."),
	 x = "Real", y = "Imaginary",
	 colour = "Iterations\nuntil divergence")
#+end_src

#+RESULTS:
[[file:nM-detail.png]]

These produce great aesthetics, though I want to test some alternative
visualisations. The following scales points based on number of
iterations:g

#+begin_src R :file nM-overall-alt.png :res 100 :height 1000 :width 1000 :results output graphics :colnames yes :session rsession1 :exports both :eval never-export
  max_n <- 40

  complex_df(-2.5, 1, 0.05, -1.5, 1.5, 0.05) %>%
    dplyr::mutate(nM = purrr::map_dbl(c, nM, max_n)) %>%
    ggplot(aes(re, im)) +
    geom_point(aes(size = nM)) +
    scale_size(trans = "sqrt") +
    coord_fixed() +
    labs(title = "Mandelbrot Set",
	 caption = glue::glue("Note: Maximum number of iterations: {max_n}.\nDivergence forced at maximum iterations."),
	 x = "Real", y = "Imaginary") + 
    theme(legend.position = "none")
#+end_src

#+RESULTS:
[[file:nM-overall-alt.png]]
